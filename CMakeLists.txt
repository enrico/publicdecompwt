cmake_minimum_required(VERSION 3.13.4)

include(GNUInstallDirs)

project(publicdecompwt VERSION 2.7.2)

add_compile_options(-fdiagnostics-color=always -Wno-long-long -fPIC)

file(GLOB PDWT_SOURCES DISE/*.cpp COMP/Src/*.cpp COMP/T4/Src/*.cpp COMP/JPEG/Src/*.cpp COMP/WT/Src/*.cpp)
file(GLOB PDWT_HEADERS DISE/*.h COMP/Inc/*.h COMP/Src/Inc/*.h COMP/T4/Inc/*.h COMP/JPEG/Inc/*.h COMP/WT/Inc/*.h)

add_library(publicdecompwt ${PDWT_SOURCES})

configure_file(libpublicdecompwt.pc.in libpublicdecompwt.pc @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libpublicdecompwt.pc DESTINATION "${CMAKE_INSTALL_LIBDIR}/pkgconfig")

target_include_directories(publicdecompwt PRIVATE DISE/ COMP/Inc/ COMP/Src/Inc/ COMP/T4/Inc/ COMP/JPEG/Inc/ COMP/WT/Inc/)

install(TARGETS publicdecompwt
	LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
        ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}")

install(FILES ${PDWT_HEADERS} DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/PublicDecompWT")
